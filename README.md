# CONWAY'S GAME OF LIFE

Different approach to Conway's Game Of Life, implemented in HTML and JavaScript.  
Supports:

* Achievements
* Statistics and Highscores
* Improvable bonuses
* Keyboard / Mouse
* Multilanguage

## BUGs


## TODO's

* Click Counter Statistics
* Click Counter => Fragments every X clicks
* Block F5 or Backspace + save progress in html5 local cache
* Tooltips for each element (Shops / Statistics)
* Overall generations counter statistic (for all games)
* Design Improvements
* Overall fragments counter statistic


## IMPLEMENTED

26.02.2016

* Added Multilanguage support - Arexan

25.02.2016

* Added the prestige shop (for players with too much fragments) - Arexan

24.02.2016:

* Exploit prevention (resetting alot) IMPLEMENTED - Arexan
* First row not working properly sometimes? FIXED - Baschan
* Shop Buttons are only updated on changes now - Arexan
* Buyable Fragment counter statistic (Displayed in History for each game) - Arexan

19.02.2016:  

* Generationbonus Rework (Scaling with updates from low to higher values) - Arexan
* Add Keyboard recognition (space + num) - Arexan
