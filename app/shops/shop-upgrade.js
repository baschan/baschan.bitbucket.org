shopsystem.shops['upgrade'] = {
	values: {
		upgrades: {
			fragmentHistory: {	
					//fragment counter history
					active: false,
					name: "shopbutton006",
					price: 3000,
					visible: false,
					displayed: false,
					buyable: false,
					element: false,
					setup: function(){
						
					}				
				},
			changeRulesOfEngagement: {	
					//change rule 0 -> 1
					active: false,
					name: "shopbutton008",
					price: 100000,
					visible: false,
					displayed: false,
					buyable: false,
					element: false,
					setup: function(){
						engine.copyRuleGenerations = 64;	
					}
				}
		}
	},
	displayInfo: function(){
		/*if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard016") + this.values.amount;
			elements.setHTML(this.values.element, info);
		}*/
	},
	priority: 99,
	visible: true,
	display: function(){
		var output = "";
	
		//shop is active
		if(this.visible){
			//check if upgrades are available
			Object.keys(this.values.upgrades).forEach(
				function(key, index) {
					var element = shopsystem.shops['upgrade'].values.upgrades[key];
					
					//check if upgrade is active 
					if(!element.active){
						//check which upgrade is buyable
						var buyable = shopsystem.values.currentFragments >= element.price;
						var action = "shopsystem.buy('upgrade-"+ key +"');";
						var text = e.texts.displayText("gui", element.name) +" - "+ element.price +" "+ e.texts.displayText("gui","ui006");
						
						if(!element.visible && buyable){
							element.visible = true;
						}
						
						//button is shown
						if(element.visible){
							output += gui.displayShopButton(buyable, action, text);
							
							//check if button is shown for the first time
							if(!element.displayed){
								element.displayed = true;
								shopsystem.values.shopButtonsChanged = true;
							}
							
							//check if buyable state has changed
							if(buyable != element.buyable){
								element.buyable = buyable;
								shopsystem.values.shopButtonsChanged = true;
							}
						}
					}
				}
			);
		}
		
		return output;
	},
	update: function(upgradeIndex){
		//shop is activated
		if(this.visible){
			var element = this.values.upgrades[upgradeIndex];
		
			//enough money
			if(shopsystem.values.currentFragments >= element.price){
				//pay
				shopsystem.values.currentFragments -= element.price
				
				//activate
				element.active = true;
				element.setup();
				
				//set statistics
				statistics.shopsystem.updatesBought++;
				
				updateMessage = e.texts.displayText("gui","shopmessage006");
				updateMessage = updateMessage.replace("###1###", e.texts.displayText("gui", element.name));
				displayProgressMessage(updateMessage);
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	}
}