shopsystem.shops['prestige'] = {
	values: {
		index: 0,
		amount: 0,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 10,
	visible: false,
	display: function(){
		var output = "";
	
		//displayed, max gridsize, buyable
		if(this.visible && shopsystem.shops['grid'].values.index == shopsystem.shops['grid'].pricing.length && this.values.amount < (engine.gridRows * engine.gridColumns)){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('prestige');";
			var text = e.texts.displayText("gui","shopbutton007") + (this.values.amount+1) +" - "+ this.pricing[this.values.index] +" "+ e.texts.displayText("gui","ui006");
			
			output += gui.displayShopButton(buyable, action, text);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard016") + this.values.amount;
			elements.setHTML(this.values.element, info);
		}
	},
	update: function(){
		//prestigeshop is activated
		if(this.visible){
			//enough money
			if(shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.amount++;
				
				updateMessage = e.texts.displayText("gui","shopmessage007");
				updateMessage = updateMessage.replace("###1###", this.values.amount);
				displayProgressMessage(updateMessage);
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-default");
				}
				this.displayInfo();
				
				//display new grid
				gui.board.refreshSquare(this.values.amount);
				
				statistics.shopsystem.updatesBought++;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		1000
	]
}