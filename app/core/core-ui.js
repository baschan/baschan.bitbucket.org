//graphical user interface object
gui = {
	setElementTextById: function(elementName, text){
		var element = document.getElementById(elementName);
		var elementType = element.nodeName;
		
		if (elementType == "INPUT"){
			element.value = text;
		} else if(elementType == "DIV"){
			element.innerHTML = text;
		}
	},
	displayShopButton: function(buyable, action, text){
		var output = "";
	
		//check if button is payable
		if(buyable){
			buttonClass = "paybutton-default";
		} else {
			buttonClass = "paybutton-default-off";
		}
	
		//display html
		output += "<div>";
			output += "<div class=\""+ buttonClass +"\" onClick=\""+ action +"\">"+ text +"</div>";
		output += "</div>";
		output += "<div class=\"clearfix\"> </div>";
		
		return output;
	},
	displayHistory: function(message){
		var elementName = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("history-container"), true, false, "historyContainerItem-default");
		elements.setHTML(elementName, message);
	},
	board: {
		refreshSquare: function(number){
			document.getElementById('square'+ getInputFieldNumber(number)).className += " prestige";
		}
	}
}

//display grid
function buildGrid(numberOfInputs){
	var output = "";
	
	for(var i = 1; i <= numberOfInputs; i++){
		var fieldNumber = getInputFieldNumber(i);
		var classes = "square-default";
		
		//display prestige fields
		if(shopsystem.shops['prestige'].visible && shopsystem.shops['prestige'].values.amount > 0 && shopsystem.shops['prestige'].values.amount >= i){
			classes += " prestige";
		}
		
		output += "\n<div id=\"square" + fieldNumber + "\" class=\""+ classes +"\" style=\"width:"+ 640/(engine.gridColumns) +"px; height:"+ 640/(engine.gridRows) +"px;\"><input type=\"hidden\" id=\"inputSquare" + fieldNumber + "\" value=0 /></div>";
	}
	
	return output;
}


//deactivate progress message
function hideProgressMessage(){
	document.getElementById("progressMessage").style.display = 'none';
}

//display progress message
function displayProgressMessage(message){
	document.getElementById("progressMessage").innerHTML = message;
	document.getElementById("progressMessage").style.display = 'block';
	
	setTimeout(hideProgressMessage, 2500);
}

//deactivate info message
function hideInfoMessage(){
	document.getElementById("infoMessage").style.display = 'none';
}

//display info message
function displayInfoMessage(message){
	document.getElementById("infoMessage").innerHTML = message;
	document.getElementById("infoMessage").style.display = 'block';
	
	engine.infoMessageTimer = setTimeout(hideInfoMessage, 5000);
}

//display scoreboard
function displayScoreboardGUI(){
	if(progress.gui.displayStatistics){
		//main statistics
		elements.setHTML(elementGenerator.getElementByAlias("gameinfo-totalgames"), engine.texts.displayText("gui","scoreboard001") + statistics.game.gamesResetAutomatically);
		elements.setHTML(elementGenerator.getElementByAlias("gameinfo-totalresets"), engine.texts.displayText("gui","scoreboard002") + statistics.game.gamesResetManually);
		
		//current fragment count
		if(progress.shopsystem.gui.displayFragments){
			var info = engine.texts.displayText("gui","scoreboard011") + shopsystem.values.currentFragments;
			elements.setHTML(elementGenerator.getElementByAlias("current-fragments"), info);
		}
		
		//display shop statistics
		if(progress.shopsystem.gui.displayFragments){
			Object.keys(shopsystem.shops).forEach(
				function(key, index) {
					var element = shopsystem.shops[key];
					
					//shop is visible
					if(element.visible){
						element.displayInfo();
					}
				}
			);
		}
	
		/*
		document.getElementById("min-ratio").innerHTML = statistics.game.lowestRatio;
		document.getElementById("max-ratio").innerHTML = statistics.game.highestRatio;
		document.getElementById("min-rounds").innerHTML = statistics.game.lowestGeneration;
		document.getElementById("max-rounds").innerHTML = statistics.game.highestGeneration;
		document.getElementById("min-rounds").innerHTML = statistics.game.lowestGeneration;
		document.getElementById("max-rounds").innerHTML = statistics.game.highestGeneration;
		
		if(progress.shopsystem.gui.displayUpdatesStatistic){
			document.getElementById("updates").innerHTML = statistics.shopsystem.updatesBought;
		}
		if(displayGoldenTurboStatistic){
			document.getElementById("goldenturbos").innerHTML = goldenTurbosUsed;
		}*/
		
		//display shops if fragments have changed
		if(shopsystem.fragmentsLastGeneration != shopsystem.values.currentFragments){
			shopsystem.displayShops();
			shopsystem.values.fragmentsLastGeneration = shopsystem.values.currentFragments;
		}
	}
}

//keyboard usage
function checkKey(e) {
    var event = window.event ? window.event : e;
    //console.log(event.keyCode)
	
	//next round
	if(event.keyCode == 13 || event.keyCode == 78 || event.keyCode == 32){
		generateFields();
	}
	
	//reset
	if(event.keyCode == 82){
		if(progress.gui.displayReset == true){
			resetGame(false);
		}
	}
	
	//autoplay
	if(event.keyCode == 65){
		if(progress.gui.displayAutoPlay == true){
			toggleAutoplay();
		}
	}
	
	//golden turbo
	if(event.keyCode == 71){
		activateGoldenTurbo();
	}
	
	//SHOPS
	if(event.keyCode == 49 || event.keyCode == 97){
		//speed
		shopsystem.buy('velocity');
	} else if(event.keyCode == 50 || event.keyCode == 98){
		//grid
		shopsystem.buy('grid');
	} else if(event.keyCode == 51 || event.keyCode == 99){
		//fragment bonus generations
		shopsystem.buy('generationbonus');
	} else if(event.keyCode == 52 || event.keyCode == 100){
		//fragment chance
		shopsystem.buy('fragmentchance');
	} else if(event.keyCode == 53 || event.keyCode == 101){
		//fragment base
		shopsystem.buy('base');
	} else if(event.keyCode == 54 || event.keyCode == 102){
		
	} else if(event.keyCode == 55 || event.keyCode == 103){
		
	} else if(event.keyCode == 56 || event.keyCode == 104){
		
	} else if(event.keyCode == 57 || event.keyCode == 105){
		
	} else if(event.keyCode == 48 || event.keyCode == 96){
		//prestige
		shopsystem.buy('prestige');
	}
}