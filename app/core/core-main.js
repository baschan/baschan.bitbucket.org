engine.functions = {
	objectExists: function(object){
		//check if the object exists
		if(typeof object != "undefined") {
			return true;
		}
		
		return false;
	}
}
e.f = engine.functions;

function initialize(){
	//add event listeners
	document.body.addEventListener('keyup', function(e) {
		checkKey();
    }, false);

	resetGame(true);
	engine.texts.initialize();
}

//display board
function generateFields(automatic){
		
	if(engine.resetNeeded) {
		if(engine.autoplayActive){
			toggleAutoplay();
		}
		return false;
	} else {
		engine.manualReset = false;
	}

	var countFields = engine.gridRows * engine.gridColumns;
	var currentValues = new Array();
	var nextValues = new Array();
	
	statistics.game.currentActiveFields = 0;
	
	//check if very lucky player
	if((Math.random()*goldenTurboChance) < 2){
		goldenTurbo = true;
		shopsystem.values.shopButtonsChanged = true;
	}
	
	//count rounds for turbo down
	if(goldenTurboGenerations > 1) {
		goldenTurboGenerations--;
		
		//after reset colorize again
		if(statistics.game.currentGenerations == 0){
			//colorize grid
			document.getElementById("board").style.border = goldenTurboGridColor;
		}
	} else if (goldenTurboGenerations == 1){
		goldenTurboGenerations--;
		
		//colorize grid
		document.getElementById("board").style.border = defaultGridColor;
		
		//set speed
		shopsystem.shops['velocity'].values.currentAutoSpeed = shopsystem.shops['velocity'].values.autoSpeedValue;
		
		//activate speed
		toggleAutoplay();
		toggleAutoplay();
	}
	
	//count rounds for copy rule
	
	if(shopsystem.shops['upgrade'].values.upgrades.changeRulesOfEngagement.active){
		if(engine.copyRuleGenerations > 1) {
			engine.copyRuleGenerations--;
		} else if (engine.copyRuleGenerations == 1){
			engine.copyRuleGenerations--;
			
			//deactivate rule
			shopsystem.shops['upgrade'].values.upgrades.changeRulesOfEngagement.active = false;
			engine.rulesOfEngagement = 0;
		}
	}
	
	
	for(var i = 1; i <= countFields; i++){
	
		//leading zeros 003
		var counter = getInputFieldNumber(i);
		
	
		//get current value
		var fieldInput = document.getElementById("inputSquare" + counter);
		
		//add to array
		
		if(statistics.game.currentGenerations != 0){
			currentValues.push(fieldInput.value);
		}else{
			//random first action
			currentValues.push(Math.round(Math.random()));
		}
	}
	
	//manipulating data
	for(var currentRow = 1; currentRow <= engine.gridRows; currentRow++){
		for(var currentColumn = 1; currentColumn <= engine.gridColumns; currentColumn++){
			//current Field
			var fieldNumber = currentColumn + ((currentRow - 1) * engine.gridRows);
			var additionalDiv = "";
			
			var debugField = 55;
			
			if(!engine.debugMode){
				debugField = 0;
			}else{
				if(fieldNumber == debugField){
					console.log("______________START LOGGING GEN "+statistics.game.currentGenerations+"_______________");
				}
			}
			
			var tictactoe = new Array();
			tictactoe[0] = 0;
			tictactoe[1] = 0;
			tictactoe[2] = 0;
			tictactoe[3] = 0;
			tictactoe[4] = 0;
			tictactoe[5] = 0;
			tictactoe[6] = 0;
			tictactoe[7] = 0;
			tictactoe[8] = 0;
			
			//center point
			tictactoe[4] = currentValues[fieldNumber - 1];
			
			
			//check alive cells around
			var countAliveNeighbours = 0;
			var	newValue = 0;
			
			//check if there are three alive cells around
			
			//start with left column
			if(currentColumn > 1){
				if(currentRow > 1){
					//top
					var checkFieldNumber = fieldNumber - engine.gridColumns - 1;
					if(currentValues[checkFieldNumber - 1] == 1){
						countAliveNeighbours++;
						tictactoe[0] = 1;
						if(fieldNumber == debugField){
							console.log("left column top row alive");
						}
					}else{
						if(fieldNumber == debugField){
							console.log("left column top row dead");
						}
					}	
				}
				
				
				//middle
				var checkFieldNumber = fieldNumber - 1;
				if(currentValues[checkFieldNumber - 1] == 1){
					countAliveNeighbours++;
					tictactoe[3] = 1;
					if(fieldNumber == debugField){
						console.log("left column middle row alive");
					}
				}else{
					if(fieldNumber == debugField){
						console.log("left column middle row dead");
					}
				}
				
				
				if(currentRow < engine.gridRows){
					//bottom
					var checkFieldNumber = fieldNumber + engine.gridColumns - 1;
					if(currentValues[checkFieldNumber - 1] == 1){
						countAliveNeighbours++;
						tictactoe[6] = 1;
						if(fieldNumber == debugField){
							console.log("left column bottom row alive");
						}
					}else{
						if(fieldNumber == debugField){
							console.log("left column bottom row dead");
						}
					}
				}					
			}
			
			//middle column
			if(currentRow > 1){
				//top
				var checkFieldNumber = fieldNumber - engine.gridColumns;
				if(currentValues[checkFieldNumber - 1] == 1){
					countAliveNeighbours++;
					tictactoe[1] = 1;
					if(fieldNumber == debugField){
						console.log("middle column top row alive");
					}
				}else{
					if(fieldNumber == debugField){
						console.log("middle column top row dead");
					}
				}
			}
			
			if(currentRow < engine.gridRows){
				//bottom
				var checkFieldNumber = fieldNumber + engine.gridColumns;
				if(currentValues[checkFieldNumber - 1] == 1){
					countAliveNeighbours++;
					tictactoe[7] = 1;
					if(fieldNumber == debugField){
						console.log("middle column bottom row alive");
					}
				}else{
					if(fieldNumber == debugField){
						console.log("middle column bottom row dead");
					}
				}
			}
			
			//right column
			if(currentColumn < engine.gridColumns){
				if(currentRow > 1){
					//top
					var checkFieldNumber = fieldNumber - engine.gridColumns + 1;
					if(currentValues[checkFieldNumber - 1] == 1){
						countAliveNeighbours++;
						tictactoe[2] = 1;
						if(fieldNumber == debugField){
							console.log("right column top row alive");
						}
					}else{
						if(fieldNumber == debugField){
							console.log("right column top row dead");
						}
					}
				}
				
				//middle
				var checkFieldNumber = fieldNumber + 1;
				if(currentValues[checkFieldNumber - 1] == 1){
					countAliveNeighbours++;
					tictactoe[5] = 1;
					if(fieldNumber == debugField){
						console.log("right column middle row alive");
					}
				}else{
					if(fieldNumber == debugField){
						console.log("right column middle row dead");
					}
				}
				
				if(currentRow < engine.gridRows){
					//bottom
					var checkFieldNumber = fieldNumber + engine.gridColumns + 1;
					if(currentValues[checkFieldNumber - 1] == 1){
						countAliveNeighbours++;
						tictactoe[8] = 1;
						if(fieldNumber == debugField){
							console.log("right column bottom row alive");
						}
					}else{
						if(fieldNumber == debugField){
							console.log("right column bottom row dead");
						}
					}
				}
			}
			
			var resultFromRule = applyRule(currentValues[fieldNumber - 1], countAliveNeighbours);
			
			if(resultFromRule === true){
				newValue = 1;
				var style = "";
				
				if(progress.shopsystem.gui.displayFragments && (Math.random()*shopsystem.shops['fragmentchance'].values.chance) < 2){
					fragmentColor = 'rgb(' + (Math.floor(Math.random() * 255)) + ','
									 + (Math.floor(Math.random() * 255)) + ','
									 + (Math.floor(Math.random() * 255)) + ')';
									 
					style = " style=\"background-color: "+ fragmentColor +";\" ";
					
					if(shopsystem.shops['generationbonus'].values.active){
						//base is active
						if(statistics.game.currentGenerations >= shopsystem.shops['generationbonus'].values.generationNeeded){
							//change color of grid
							var boardSquareList = document.querySelectorAll(".square-default");
 
							for (var row = 0; row < boardSquareList.length; row++) {
								boardSquareList[row].style.border = "1px solid "+fragmentColor;
							}
							
							gridColorNotDefault = true;
						} else if(gridColorNotDefault) {
							//change color of grid
							var boardSquareList = document.querySelectorAll(".square-default");
 
							for (var i = 0; i < boardSquareList.length; i++) {
								boardSquareList[i].style.border = "1px solid "+fragmentDefaultColor;
							}
							
							gridColorNotDefault = false;
						}
					}
					
					shopsystem.values.currentFragments += shopsystem.getFragmentsPerGeneration(false, true);
					displayScoreboardGUI();
					
					document.getElementById("money-icon").style.color = fragmentColor;
				}
				
				additionalDiv = "<div class=\"black\" "+ style +"></div>";
			}	
			
			if(fieldNumber == debugField){
				console.log("fieldNumber = "+fieldNumber +", currentRow = "+currentRow+", currentColumn = "+currentColumn);
				console.log("countAliveNeighbours = "+countAliveNeighbours);
				console.log("is now alive: "+newValue);
				console.log("tictactoe before:");
				var outputRow = "";
				for(var tic = 0; tic < tictactoe.length; tic++){
					if(tictactoe[tic] == 1){
						outputRow += "|X|";
					}else{
						outputRow += "|O|";
					}

					if((tic + 1) % 3 == 0){
						console.log(outputRow);
						outputRow = "";
					}
				}
				
			}
			
			
			//first action -> return random to check
			if(statistics.game.currentGenerations == 0){
				if(currentValues[fieldNumber] == 1){
					newValue = 1;
					additionalDiv = "<div class=\"black\"></div>";
				}else{
					newValue = 0;
					additionalDiv = "";
				}
			}
			if(newValue == 1){
				statistics.game.currentActiveFields++;
			}
			
			nextValues.push(newValue);
		
			var fieldElement = document.getElementById("square" + getInputFieldNumber(fieldNumber));
			fieldElement.innerHTML = "<input type=\"hidden\" id=\"inputSquare" + getInputFieldNumber(fieldNumber) + "\" value=" + newValue + " />" + additionalDiv;
		}
	}
	
	if(engine.gridHistory.length == 7){
		engine.gridHistory.shift();
	}
	engine.gridHistory.push(currentValues.toString());
	
	statistics.game.currentGenerations++;
	if(automatic == true){
		statistics.game.automaticallyClicked++;
	} else {
		statistics.game.manuallyClicked++;
	}
	
	//generation
	if(progress.gui.displayGenerationsInfo){
		elements.setHTML(elementGenerator.getElementByAlias("info-generation"), e.texts.displayText("gui","ui001") + statistics.game.currentGenerations);
	}
	//game info
	if(progress.gui.displayCurrentGameInfo){
		statistics.game.currentRatio = roundNumber(statistics.game.currentActiveFields / (engine.gridRows * engine.gridColumns) * 100, 2);
		elements.setHTML(elementGenerator.getElementByAlias("info-currentgame"), e.texts.displayText("gui","ui002") + statistics.game.currentActiveFields + " / " + e.gridRows * e.gridColumns + " | "+ e.texts.displayText("gui","ui003") + getRatioHtml(statistics.game.currentRatio, "none"));
	}
	
	//check progress
	checkProgress();
	
	if(statistics.game.currentActiveFields == 0){
		e.resetNeeded = true;
		elements.setFocus(elementGenerator.getElementByAlias("button-reset"), false);
		
		if(progress.engine.autoReset){
			resetGame(true)
		} else {
			displayInfoMessage(e.texts.displayText("gui","message002"));
		}
	} 
	//infinite loop
	else if(e.gridHistory[0] == e.gridHistory[1] || e.gridHistory[0] == e.gridHistory[2] || e.gridHistory[0] == e.gridHistory[6] || e.gridHistory[0] == e.gridHistory[3]){
		e.resetNeeded = true;
		
		if(progress.engine.autoReset){
			resetGame(true)
		} else {
			elements.setFocus(elementGenerator.getElementByAlias("button-reset"), false);
			displayInfoMessage(e.texts.displayText("gui","message001"));
		}
	}
	
	displayScoreboardGUI();
	return true;
}


function generateFieldsAuto(){
	generateFields(true);
}

function nextGeneration(){
	generateFields(false);
}

function newGame(){
	resetGame(false);
}

//autoplay interval
function autoplay(val) {
	if(progress.gui.displayAutoPlay) {
		if (val == "run") {
			e.autoplayActive = true;
			e.autoplayTimer = window.setInterval(generateFieldsAuto, shopsystem.shops['velocity'].values.currentAutoSpeed);
		} else {
			e.autoplayActive = false;
			window.clearInterval(e.autoplayTimer);
		}
	}
}

//toggle autoplay
function toggleAutoplay() {
	if(progress.gui.displayAutoPlay) {
		if(!e.autoplayActive) {
			autoplay("run");
			
			elements.unsetActive(elementGenerator.getElementByAlias("button-autoplay"), false);
			
			//document.getElementById("autoplay").className = document.getElementById("autoplay").className.replace("-off","");
		} else {
			autoplay("stop");
			elements.setActive(elementGenerator.getElementByAlias("button-autoplay"), false);
			//document.getElementById("autoplay").className += "-off";
		}
	}
} 

function roundNumber(rnum, rlength) {
  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
  return parseFloat(newnumber);
}

//reset the game and store highscores
function resetGame(automatic){
	e.resetNeeded = false;
	e.gridHistory = new Array();

	var ratioText = "";
	ratioText = statistics.game.currentRatio + "%";
	
	var countActionsText = statistics.game.currentGenerations;
	
	//hide info message
	hideInfoMessage();
	clearTimeout(e.infoMessageTimer);
	
	//cheat detection
	if(statistics.game.gamesOverall > 0){
		cheatProtection.checkGenerations();
	}
	shopsystem.values.updateCall = false;
	
	//scores only with automatic reset (no cheating)
	if(automatic){
		//first round => set all statistics
		if(e.firstGeneration == true){
			e.firstGeneration = false;
			statistics.game.gamesResetAutomatically++;
		} else if(statistics.game.currentGenerations > 1){
			if (statistics.game.lowestRatio == 0 && statistics.game.highestRatio == 0) {
				statistics.game.lowestRatio = statistics.game.currentRatio;
				statistics.game.highestRatio = statistics.game.currentRatio;
			} 
			
			if (statistics.game.currentRatio < statistics.game.lowestRatio) {
				statistics.game.lowestRatio = statistics.game.currentRatio;
				ratioText = getRatioHtml(statistics.game.currentRatio, "low");
			}
			
			if(statistics.game.currentRatio > statistics.game.highestRatio){
				statistics.game.highestRatio = statistics.game.currentRatio;
				ratioText = getRatioHtml(statistics.game.currentRatio, "high");
			}
			
			if (statistics.game.highestGeneration == 0 && statistics.game.lowestGeneration == 0) {
				statistics.game.highestGeneration = statistics.game.currentGenerations;
				statistics.game.lowestGeneration = statistics.game.currentGenerations;
			} else if (statistics.game.currentGenerations > statistics.game.highestGeneration) {
				statistics.game.highestGeneration = statistics.game.currentGenerations;
				countActionsText = "<span style=\"color: \#90EE90; font-weight:bold;\">"+ statistics.game.currentGenerations +"</span>"
			} else if(statistics.game.currentGenerations < statistics.game.lowestGeneration){
				statistics.game.lowestGeneration = statistics.game.currentGenerations;
				countActionsText = "<span style=\"color: indianred; font-weight:bold;\">"+ statistics.game.currentGenerations +"</span>"
			}
		}
	}
		
	var message = "<div class=\"generation\">"+ e.texts.displayText("gui","ui005") +"<div class=\"amount\">"+ countActionsText +"</div></div><div class=\"lives\">"+ statistics.game.currentActiveFields +"<div class=\"square\"></div></div><div class=\"ratio\">"+ ratioText +"</div>";
	
	//fragment history + bonus fragments
	if(progress.shopsystem.gui.displayFragments){
		if(shopsystem.shops['upgrade'].values.upgrades.fragmentHistory.active){
			if(statistics.game.currentRatio == 0){
				//calculate fragment base aswell
				message += "<div class=\"fragmentBonus\">"+ statistics.shopsystem.fragmentsLastGame +" (+"+ (shopsystem.getFragmentsPerGeneration(true, false)) +") "+ e.texts.displayText("gui","ui006") +"</div>";
				shopsystem.values.currentFragments += shopsystem.getFragmentsPerGeneration(true, true);
			} else {
				//display only fragments for this game
				message += "<div class=\"fragmentBonus\">"+ statistics.shopsystem.fragmentsLastGame +" "+ e.texts.displayText("gui","ui006") +"</div>";
			}
		} else if (statistics.game.currentRatio == 0){
			//calculate fragment base aswell
			message += "<div class=\"fragmentBonus\">"+ (shopsystem.getFragmentsPerGeneration(true, false)) +" "+ e.texts.displayText("gui","ui006") +"</div>";
			shopsystem.values.currentFragments += shopsystem.getFragmentsPerGeneration(true, true);
		}
	}
	
	if(automatic == false){
		message = message + "<div class=\"message\">"+ e.texts.displayText("gui","ui004") +"</div>";
		e.manualReset = true;
	}
	
	message = message + "<div style=\"clear: both;\"></div>";
	
	//display history
	if(progress.gui.displayHistory && statistics.game.gamesResetManually != 0){
		gui.displayHistory(message);
	}
	
	//reset fragment count for game
	statistics.shopsystem.fragmentsLastGame = 0;
	
	//game not manually boosted
	if(e.manualReset){
		statistics.game.gamesResetManually++;
	} else {
		statistics.game.gamesResetAutomatically++;
	}
	statistics.game.gamesOverall++;

	//display grid
	//document.getElementById('board').setAttribute("style","width: "+ e.gridColumns*20 +"px; height: "+ e.gridRows*20 +"px");
	document.getElementById('board').innerHTML = buildGrid(e.gridRows * e.gridColumns);
	statistics.game.currentGenerations = 0;
	
	//change reset button
	if(progress.gui.displayReset){
		elements.unsetFocus(elementGenerator.getElementByAlias("button-reset"), false);
	}
	
	//display scoreboard
	displayScoreboardGUI();
}

//reset game statistics
function resetStatistics(){
	//engine.firstGeneration = true;
	statistics.game.lowestRatio = 0;
	statistics.game.highestRatio = 0;
	statistics.game.highestGeneration = 0;
	statistics.game.lowestGeneration = 0;
	
	//reset history
	elements.setHTML(elementGenerator.getElementByAlias("history-container"), "");
}

function getInputFieldNumber(number){
	var output = "";
	//leading zeros 003
	if(number < 10){
		output = "00" + number;
	}else if(number < 100){
		output = "0" + number;
	}else{
		output = number;
	}

	return output;
}

//get ratio color
function getRatioHtml(ratio, type){
	var ratioText = "";
	
	if (type == "low") {
		ratioText = "<span style=\"color: indianred;font-weight:bold;\">" + ratio + "%</span>";
	} else if (type == "high") {
		ratioText = "<span style=\"color: \#90EE90;font-weight:bold;\">" + ratio + "%</span>";
	} else {
		if (ratio < 10){
			ratioText = "<span style=\"color: indianred;font-weight:bold;\">" + ratio + "%</span>";
		}else{
			ratioText = ratio + "%";
		}
	}
	
	return ratioText;
}

//check progress
function checkProgress(){
	//display reset after 1 click
	if(statistics.game.manuallyClicked >= 1 && !progress.gui.displayReset){
		progress.gui.displayReset = true;
		
		//add element
		var elementName = elementGenerator.addElement("buttons", false, "button-reset");
		elements.setHTML(elementName, engine.texts.displayText("gui","button003"));
		elements.setClasses(elementName, "button03-default");
		elements.addEventListener(elementName, 'mouseup', "newGame");
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage001"));
	}
	
	//display rounds after 10 click
	if(statistics.game.manuallyClicked >= 10 && !progress.gui.displayGenerationsInfo){
		progress.gui.displayGenerationsInfo = true;
		
		//add element
		var elementName = elementGenerator.addElement("page-center", false, "info-generation");
		elements.addClass(elementName, "infoGeneration-default");
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage002"));		
	}
	
	//display game info after 25 click
	if(statistics.game.manuallyClicked >= 25 && !progress.gui.displayCurrentGameInfo){
		progress.gui.displayCurrentGameInfo = true;
		
		//add element
		var elementName = elementGenerator.addElement("page-center", false, "info-currentgame");
		elements.setClasses(elementName, "infoCurrentGame-default");
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage003"));
	}
	
	//display auto play after 50 click
	if(statistics.game.manuallyClicked >= 50 && !progress.gui.displayAutoPlay){
		progress.gui.displayAutoPlay = true;
		
		var elementName = elementGenerator.addElement("buttons", false, "button-autoplay");
		elements.setHTML(elementName, engine.texts.displayText("gui","button002"));
		elements.setClasses(elementName, "button02-default");
		elements.addEventListener(elementName, 'mouseup', "toggleAutoplay");
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage004"));
	}
	
	//activate auto reset after 3 resets
	if(statistics.game.gamesResetManually >= 3 && !progress.engine.autoReset){
		progress.engine.autoReset = true;
		displayProgressMessage(e.texts.displayText("gui","progressmessage005"));
	}
	
	//display history
	if(statistics.game.gamesResetAutomatically >= 3 && !progress.gui.displayHistory){
		progress.gui.displayHistory = true;
		
		//add element
		var elementName = elementGenerator.addElement("page-left", false, "history");
		elements.addClass(elementName, "history-default");
		
		//add header
		var headerElementName = elementGenerator.addElementWithClassname(elementName, false, "history-header", "historyHeader-default");
		elements.setHTML(headerElementName, engine.texts.displayText("gui", "ui008"));
		
		//add history container
		var containerElementName = elementGenerator.addElement(elementName, false, "history-container");
		elements.addClass(containerElementName, "historyContainer-default");
		
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage006"));
	}
	
	//display highscores
	if(statistics.game.gamesResetAutomatically >= 6 && !progress.gui.displayStatistics){
		progress.gui.displayStatistics = true;
		
		//add element
		var elementName = elementGenerator.addElement("page-right", false, "scoreboard");
		elements.addClass(elementName, "scoreboard-default");
		
		//add header
		var headerElementName = elementGenerator.addElement(elementName, false, "scoreboard-header");
		elements.addClass(headerElementName, "scoreboardHeader-default");
		elements.setHTML(headerElementName, engine.texts.displayText("gui", "ui009"));
		
		//add main statistics
		elementGenerator.addElementWithClassname(elementName, false, "gameinfo-totalgames", "scoreboardItem-default");
		elementGenerator.addElementWithClassname(elementName, false, "gameinfo-totalresets", "scoreboardItem-default");
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage007"));
	}
	
	//display fragments
	if(statistics.game.gamesResetAutomatically >= 9 && !progress.shopsystem.gui.displayFragments){
		progress.shopsystem.gui.displayFragments = true;
		
		//add fragment statistic
		var elementName = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, "current-fragments", "scoreboardItem-default");
		elements.setHTML(elementName, shopsystem.values.currentFragments);
		
		//add element for shop
		elementGenerator.addElement("page-right", false, "shopsystem-buttons");
		
		displayProgressMessage(e.texts.displayText("gui","progressmessage008"));
	}
	
	//check shop progress
	shopsystem.checkProgress();
	
	//updates counter
	if(statistics.shopsystem.updatesBought > 0 && !progress.shopsystem.gui.displayUpdatesStatistic){
		progress.shopsystem.gui.displayUpdatesStatistic = true;
	}
}

function applyRule(field, countAliveNeighbours){
	var result = false;
	
	var alive = true;
	var dead = false;
	
	if(field == 0){
		alive = false;
		dead = true;
	}
	
	var gridMax = (shopsystem.shops['grid'].values.index == shopsystem.shops['grid'].pricing.length);
	
	//RULES TO BE ALIVE:
	
	if(engine.rulesOfEngagement == 3){
		
		//dead: exactly three alive neighbours
		//alive: three or four alive neighbours
		
		//34/3 rule
	
		if((dead && countAliveNeighbours == 3) || (alive && (countAliveNeighbours == 3 || countAliveNeighbours == 4))){
			result = true;
		}
		
	}else if(engine.rulesOfEngagement == 3){
		
		//dead: exactly three alive neighbours
		//alive: three alive neighbours
		
		//13/3 rule
	
		if((dead && countAliveNeighbours == 3) || (alive && (countAliveNeighbours == 1 || countAliveNeighbours == 3))){
			result = true;
		}
		
	}else if(engine.rulesOfEngagement == 2){
		
		//dead: exactly three alive neighbours
		//alive: three alive neighbours
		
		//3/3 rule
	
		if((dead && countAliveNeighbours == 3) || (alive && countAliveNeighbours == 3)){
			result = true;
		}
		
	}else if(engine.rulesOfEngagement == 1 && gridMax){
		
		//COPY
		//dead: even number of neighbours (including 0)
		//alive: odd number of neighbours
		// + grid maximum
	
		if(countAliveNeighbours % 2){
			result = true;
		}
		
	}else{
		
		//DEFAULT
		//dead: exactly three alive neighbours
		//alive: two or three alive neighbours
		
		if((dead && countAliveNeighbours == 3) || (alive && (countAliveNeighbours == 2 || countAliveNeighbours == 3))){
			result = true;
		}
	} 
		
	
	return result;
}