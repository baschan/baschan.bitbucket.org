engine.texts = {
	initialize: function(){
		//title
		document.title = this.displayText("game","title");
		gui.setElementTextById("title",this.displayText("game","title"));
	
		//buttons
		gui.setElementTextById("click",this.displayText("gui","button001"));
	},
	changeLanguage: function(languageCode){
		//check if language is given
		if(e.f.objectExists(this.values[languageCode])){
			//change language
			this.values.language = languageCode;
			
			//reinitialize language
			this.initialize();
		}
	},
	displayText: function(group, identifier){
		return this.values[this.values.language][group][identifier];
	},
	values: {
		language: "en",
		en: {
			//game
			game: {
				"title": "Conway's Game of Life"
			},
			//gui
			gui: {
				"button001": "next generation (n)",
				"button002": "toggle autoplay (a)",
				"button003": "reset game (r)",
				
				"scoreboard001": "Games played:",
				"scoreboard002": "Games reset:",
				"scoreboard003": "Max Generations:",
				"scoreboard004": "Min Generations:",
				"scoreboard005": "Max Ratio:",
				"scoreboard006": "Min Ratio:",
				"scoreboard007": "Velocity:",
				"scoreboard008": "Grid Size:",
				"scoreboard009": "Updates:",
				"scoreboard010": "Golden-Turbos:",
				"scoreboard011": "Fragments: <i id=\"money-icon\" class=\"fa fa-money\"></i>:",
				"scoreboard012": "Chance:",
				"scoreboard013": "Bonus every:",
				"scoreboard014": "Per Generation:",
				"scoreboard015": "Fragment Base:",
				"scoreboard016": "Prestige Level:",
				
				"ui001": "Generation: ",
				"ui002": "Fields alive: ",
				"ui003": "ratio: ",
				"ui004": "Manual reset!",
				"ui005": "Generation ",
				"ui006": "<i class=\"fa fa-money\"></i>",
				"ui007": "(G) Golden Turbo - BONUS",
				"ui008": "History",
				"ui009": "Statistic",
				
				"message001": "Infinite loop, please reset game.",
				"message002": "No lives left, please reset game.",
				
				"progressmessage001": "Reset Button Unlocked!",
				"progressmessage002": "Generations Unlocked!",
				"progressmessage003": "Game Info Unlocked!",
				"progressmessage004": "Autoplay Unlocked!",
				"progressmessage005": "Auto Reset Unlocked!",
				"progressmessage006": "History Unlocked!",
				"progressmessage007": "Scoreboard Unlocked!",
				"progressmessage008": "Fragments Unlocked!",
				
				"shopbutton001": "Velocity Boost ",
				"shopbutton002": "Extend Grid ",
				"shopbutton003": "Generation Bonus ",
				"shopbutton004": "Fragment Chance ",
				"shopbutton005": "Fragment Base ",
				"shopbutton006": "Fragment History ",
				"shopbutton007": "(0) Buy Prestige Field ",
				"shopbutton008": "Change Rules Of Engagement ",
				
				"shopmessage001": "Speedboost ###1### Unlocked!",
				"shopmessage002": "Extend Grid ###1### Unlocked (Statistics Reset)!",
				"shopmessage003": "Generation Bonus ###1### Unlocked!",
				"shopmessage004": "Fragment Chance ###1### Unlocked!",
				"shopmessage005": "Fragment Base ###1### Unlocked!",
				"shopmessage006": "###1### Unlocked!",
				"shopmessage007": "Prestige ###1### Unlocked!",
				
				
				"cheatprotection001": "Fragment Generation stopped for ###1### Game/s!"
			}
		},
		de: {
			//game
			game: {
				"title": "Conway's Spiel des Lebens"
			},
			//gui
			gui: {
				"button001": "Naechste Generation (n)",
				"button002": "Aktiviere Automatik (a)",
				"button003": "Neustart (r)",
				
				"scoreboard001": "Spiele:",
				"scoreboard002": "Neustarts:",
				"scoreboard003": "Max Generationen:",
				"scoreboard004": "Min Generationen:",
				"scoreboard005": "Max Ratio:",
				"scoreboard006": "Min Ratio:",
				"scoreboard007": "Geschwindigkeit:",
				"scoreboard008": "Spielfeld:",
				"scoreboard009": "Verbesserungen:",
				"scoreboard010": "Goldene-Turbos:",
				"scoreboard011": "Fragmente: <i id=\"money-icon\" class=\"fa fa-money\"></i>",
				"scoreboard012": "Chance:",
				"scoreboard013": "Bonus alle:",
				"scoreboard014": "Pro Generation:",
				"scoreboard015": "Fragment Basis:",
				"scoreboard016": "Prestige Level:",
				
				"ui001": "Generation: ",
				"ui002": "Felder lebendig: ",
				"ui003": "ratio: ",
				"ui004": "Manueller neustart!",
				"ui005": "Generation ",
				"ui006": "<i class=\"fa fa-money\"></i>",
				"ui007": "(G) Goldener Turbo - BONUS",
				"ui008": "Historie",
				"ui009": "Statistik",
				
				"message001": "Unendliches Spiel, bitte neustarten!",
				"message002": "Keine Lebenden Felder uebrig, bitte neustarten.",
				
				"progressmessage001": "Neustarten freigeschaltet!",
				"progressmessage002": "Generationen freigeschaltet!",
				"progressmessage003": "Spiel Info freigeschaltet!",
				"progressmessage004": "Automatisches spielen freigeschaltet!",
				"progressmessage005": "Automatisches neustarten freigeschaltet!",
				"progressmessage006": "Historie freigeschaltet!",
				"progressmessage007": "Fortschrittanzeige freigeschaltet!",
				"progressmessage008": "Fragmente freigeschaltet!",
				
				"shopbutton001": "Geschwindigkeits Schub ",
				"shopbutton002": "Spielfeld Erweiterung ",
				"shopbutton003": "Generations Bonus ",
				"shopbutton004": "Fragment Chance ",
				"shopbutton005": "Fragment Basis ",
				"shopbutton006": "Fragment Historie ",
				"shopbutton007": "(0) Prestige Feld kaufen ",
				"shopbutton008": "Rules Of Engagement ändern ",
				
				"shopmessage001": "Geschwindigkeits Bonus ###1### freigeschaltet!",
				"shopmessage002": "Spielfeld Erweiterung ###1### freigeschaltet (Statistik zurueckgesetzt)!",
				"shopmessage003": "Generations Bonus ###1### freigeschaltet!",
				"shopmessage004": "Fragment Chance ###1### freigeschaltet!",
				"shopmessage005": "Fragment Basis ###1### freigeschaltet!",
				"shopmessage006": "###1### freigeschaltet!",
				"shopmessage007": "Prestige ###1### freigeschaltet!",
				
				
				"cheatprotection001": "Fragment Generierung fuer ###1### Spiel/e gestoppt!"
			}
		}
	}
}