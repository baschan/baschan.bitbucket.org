var elements = {
	addEventListener: function(id, event, action){
		document.getElementById(id).addEventListener(event, function(e) {
			window[action]();
		}, false);
	},
	addClass: function(id, value){
		document.getElementById(id).className += value;
	},
	setActive: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname+"-off", classname);
		} else {
			document.getElementById(id).className = document.getElementById(id).className.replace("-off", "");
		}
	},
	setClasses: function(id, value){
		document.getElementById(id).className = value;
	},
	setFocus: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname, classname+"-focus");
		} else {
			document.getElementById(id).className += "-focus";
		}
	},
	setHTML: function(id, value){
		//if the value changed
		if(document.getElementById(id).innerHTML != value){
			document.getElementById(id).innerHTML = value;
		}
	},
	unsetActive: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname, classname+"-off");
		} else {
			document.getElementById(id).className += "-off";
		}
	},
	unsetFocus: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname+"-focus", classname);
		} else {
			document.getElementById(id).className = document.getElementById(id).className.replace("-focus", "");
		}
	}
}