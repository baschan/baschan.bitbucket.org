var elementGenerator = {
	values: {
		newestPosition: 1
	},
	aliases: {
	
	},
	addElement: function(position, prepend, alias){
		var editableElement = document.getElementById(position);
		var newElementName = "element-"+ this.values.newestPosition;
		
		elements[newElementName]
		
		var newElement = "<div id=\""+ newElementName +"\"></div>";
		this.values.newestPosition++;
		
		//prepend or append
		if(prepend){
			editableElement.innerHTML = newElement + editableElement.innerHTML;
		} else {
			editableElement.innerHTML += newElement;
		}
		
		//save alias for later use
		if(alias != false){
			this.aliases[alias] = newElementName;
		}
		
		//return the new element
		return newElementName;
	},
	addElementWithClassname: function(position, prepend, alias, classname){
		var elementName = elementGenerator.addElement(position, prepend, alias);
		elements.addClass(elementName, classname);
		
		return elementName;
	},
	getElementByAlias: function(alias){
		var returnElement = this.aliases[alias]
		
		if(returnElement != "undefined"){
			return returnElement;
		} else {
			return -1;
		}
	}
}